package com.ridit.java.CountdownLatch;

import java.util.concurrent.CountDownLatch;

public class Class3D {
	public static final int NUMBER_OF_STUDENT = 4;
	
	CountDownLatch latch = new CountDownLatch(NUMBER_OF_STUDENT);
	
	MyStudentTask taskRidit = new MyStudentTask("Ridit", 10000, latch);
	MyStudentTask taskSidharth = new MyStudentTask("Sidharth", 1000, latch);
	MyStudentTask taskRiddhiman = new MyStudentTask("Riddhiman", 100, latch);
	MyStudentTask taskRehansh = new MyStudentTask("Rehansh", 100, latch);
	
	Thread Ridit = new Thread(taskRidit);
	Thread Sidharth = new Thread(taskSidharth);
	Thread Riddhiman = new Thread(taskRiddhiman);
	Thread Rehansh = new Thread(taskRehansh);
	
	public void giveTaskToStudents() {
		
		Ridit.start();
		Sidharth.start();
		Riddhiman.start();
		Rehansh.start();
		System.out.println("Teacher is waiting for all the students to finish the task");
		try {
			
			latch.await();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("All the students have finished. Ma'am can leave now");
	}

}


