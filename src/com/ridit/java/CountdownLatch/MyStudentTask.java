package com.ridit.java.CountdownLatch;

import java.util.concurrent.CountDownLatch;

public class MyStudentTask implements Runnable{
	private String name;
	private int timeToFinshed;
	private CountDownLatch latch;
	
	public MyStudentTask(String name,int timeToFinshed,CountDownLatch latch) {
		this.name = name;
		this.timeToFinshed = timeToFinshed;
		this.latch = latch;
		
	}


	@Override
	public void run() {
		// TODO Auto-generated method stub
		System.out.println(this.name + " has started the classwork");
		try {
			Thread.sleep(timeToFinshed);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.println(this.name + " has finished the classwork");
		this.latch.countDown();
		
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
